# SAÉ de 3A FA (+ cherchants)

Voici les règles qui encadrent la réalisation de vos SAÉ pendant votre troisième année :

- Les deux SAÉ sont fusionnées autour du même projet, soit 110h.
- Les créneaux de travail (présence obligatoire et facturée à l'entreprise) sont les créneaux à l'emploi du temps.
- Les étudiants doivent proposer des projets par équipes de 4 minimum à 5 maximum *formés au sein d'un même groupe de TP*.
- Les étudiants doivent trouver un enseignant qui accepte d'encadrer leur projet.
- Le créneau du 05/09 à 13h30 permet aux étudiants de former les équipes et de réfléchir à leur projet. Ils doivent rédiger un document présentant les enjeux de leur projet ainsi que son contexte. Ce document servira à démarcher les enseignants. Ce démarchage devra être réalisé par les étudiants eux-mêmes.
- Si aucun enseignant n'est intéressé, il faudra modifier le projet jusqu'à ce qu'un enseignant accepte.
- Le document (*en PDF*) et le nom de l'enseignant encadrant devra être envoyé à [Laurent PROVOT](mailto:laurent.provot@uca.fr) pour acter le démarrage du projet.
- La soutenance terminale aura lieu le mardi 24 juin 2025.
- Pour vous aider dans votre planification, voici la répartition hebdomadaire du volume de vos SAÉ sur l'année :

| S37 | S38 | S39 | S40 | S41 | S42 | :factory: | S2 | S3 | S4 | S5 | S6 | S7 | :factory: | S20 | S21 | S22 | S23 | S24 | S25 | S26 |
| --- | --- | --- | --- | --- | --- | --------- | -- | -- | -- | -- | -- | -- | --------- | --- | --- | --- | --- | --- | --- | --- |
|  8h |  4h |  6h |  8h |  6h |  8h | :factory: | 6h | 4h | 4h | 6h | 8h | 2h | :factory: |  6h |  6h |  0h |  8h |  4h |  8h |  8h |

### Management de projet

L'équipe pédagogique gérant cette partie a pris soin de rédiger des documents précis qui vous permettront de connaître leurs attentes. *Lisez-les bien !*

- [Attentes et consignes](Documents/GestionProjet_Consignes3A.pdf)



## Projets que vous pouvez proposer aux enseignants si vous n'avez pas d'inspiration

1. [Maudit Mot Dit](https://codefirst.iut.uca.fr/git/SAE_2A_3A/Projets_SAE_3A_FI/src/branch/master/Projets/Projet_04.md)
2. [Chat2test](https://codefirst.iut.uca.fr/git/SAE_2A_3A/Projets_SAE_3A_FI/src/branch/master/Projets/Projet_05.md)
3. [Infopoly](https://codefirst.iut.uca.fr/git/SAE_2A_3A/Projets_SAE_3A_FI/src/branch/master/Projets/Projet_06.md)
4. [BibiApp](https://codefirst.iut.uca.fr/git/SAE_2A_3A/Projets_SAE_3A_FI/src/branch/master/Projets/Projet_08.md)
5. [ChatIUT](https://codefirst.iut.uca.fr/git/SAE_2A_3A/Projets_SAE_3A_FI/src/branch/master/Projets/Projet_10.md)


## Liste des projets acceptés

1. [Optifit](Projets/Projet_01.pdf) : Olivier GUINALDO  (début : 08/09/2024)
   + Tony FAGES (PM3)
   + Vianney JOURDY (PM3)
   + Louis LABORIE (PM3)
   + Anthony RICHARD (PM3)
   + Léo TUAILLON (PM3)

2. [LYKA](Projets/Projet_02.pdf) : Pascal LAFOURCADE (début : 09/09/2024)
   + Renaud BEURET (PM1)
   + Clément CHIEU (PM1)
   + Mathis DELAGE (PM1)
   + Corentin LEMAIRE (PM1)

3. [Ubongo](Projets/Projet_03.pdf) : Pascal LAFOURCADE (début : 10/09/2024)
   + Félix COURBON (PM2)
   + ~~Maël DAIM (PM2)~~
   + Erwan MÉNAGER (PM2)
   + Hugo ODY (PM2)
   + Guillaume REY (PM2)

4. [FloraFauna GO](Projets/Projet_04.pdf) : Matthieu RESTITUITO (début : 10/09/2024)
   + Patrick BRUGIÈRE (PM1)
   + Yoan BRUGIÈRE (PM1)
   + David D'ALMEIDA (PM1)
   + Alexis LAURENT (PM1)
   + Thomas MUZARD (PM1)

5. [KAmp](Projets/Projet_05.pdf) : Marc CHEVALDONNÉ (début : 11/09/2024)
   + Ema ALBISSON (PM3)
   + Kévin MONTEIRO (PM3)
   + Johnny RATTON (PM3)
   + Maxime SAPOUNTZIS (PM3)

6. [Édition de texte collaboratif avec une IA](Projets/Projet_06.pdf) : Maxime PUYS (début : 11/09/2024)
   + Evann ABRIAL (WEB2)
   + Lola CHALMIN (WEB2)
   + Roxane ROSSETTO (WEB2)

7. [POGOS](Projets/Projet_07.pdf) : Matthieu RESTITUITO (début : 12/09/2024)
   + Flavien BOUHAMDANI (WEB1)
   + Lilian BRETON (WEB1)
   + Baptiste DUDONNÉ (WEB1)
   + Maxime POINT (WEB1)
   + Clément VERDOIRE (WEB1)

8. [Bet-athlon](Projets/Projet_08.pdf) : Marc CHEVALDONNÉ (début : 12/09/2024)
   + Adam BONAFOS (PM1)
   + Simon GUILLOT (PM1)
   + Titouan LOUVET (PM1)
   + ~~Maxime VIMPÈRE (PM1)~~

9. [NutriMate](Projets/Projet_09.pdf) : Maxime PUYS (début : 13/09/2024)
   + Samuel BÉRION (WEB2)
   + Cléo EIRAS (WEB2)
   + Raphaël LACOTE (WEB2)
   + Mathis MOULIN (WEB2)
   + ~~Jade VAN BRABANDT (WEB2)~~

10. [YumQuest](Projets/Projet_10.pdf) : Anaïs DURAND (début 16/09/2024)
    + Shana CASCARA (WEB1)
    + Jérémy DUCOURTHIAL (WEB1)
    + Antoine PINAGOT (WEB1)
    + ~~Matheo THIERRY (WEB1)~~

11. [Harmonies](Projets/Projet_11.pdf) : Florent FOUCAUD (début 16/09/2024)
    + Nicolas BLONDEAU (PM3)
    + Yann CHAMPEAU (PM3)
    + Victor GABORIT (PM3)
    + ~~François YOUSSE (PM3)~~

12. [FlashUCA](Projets/Projet_12.pdf) : Cédric BOUHOURS (début 16/09/2024)
    + Dylan CHAUVET (PM2)
    + Antoine JOURDAIN (PM2)
    + Louison HORIOT (PM2)
    + Benjamin PACZKOWSKI (PM2)

13. [Cochons aigris](Projets/Projet_13.pdf) : Pascal LAFOURCADE (début : 17/09/2024)
    + Alexandre AGOSTINHO (WEB2)
    + Ludovic CASTIGLIA (WEB2)
    + Lucie GOIGOUX (WEB2)
    + Maxime ROUDIER (WEB2)
    + Noa SILLARD (WEB2)

14. [Naval War](Projets/Projet_14.pdf) : François DELOBEL (début : 17/09/2024)
    + Vivien DUFOUR (PM2)
    + Mathieu GROUSSEAU (PM2)
    + Matthias NORDEST (PM2)
    + Victor SOULIER (PM2)

15. [Memory Map](Projets/Projet_15.pdf) : François DELOBEL (début : 22/09/2024)
    + Alexis FERON (WEB1)
    + Mathis FRAMIT (WEB1)
    + Alix JEUDI--LEMOINE (WEB1)
    + Maxence JOUANNET (WEB1)

